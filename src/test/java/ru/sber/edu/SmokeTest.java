package ru.sber.edu;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import ru.sber.edu.v1.controllers.*;

import javax.inject.Inject;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
public class SmokeTest {

    @Inject
    AdminControllerRE adminControllerRE;

    @Inject
    ApplicationControllerRE applicationControllerRE;

    @Inject
    CreditInspectorControllerRE creditInspectorControllerRE;

    @Inject
    KafkaControllerRE kafkaControllerRE;

    @Inject
    RegistrationControllerRE registrationControllerRE;

    @Inject
    UnderWriterControllerRE underWriterControllerRE;

    @Inject
    UserControllerRE userControllerRE;

    @Test
    public void whenContextLoadThenAdminControllerNotNull() throws Exception {
        assertThat(adminControllerRE).isNotNull();
    }


    @Test
    public void whenContextLoadThenApplicationControllerNotNull() throws Exception {
        assertThat(applicationControllerRE).isNotNull();
    }


    @Test
    public void whenContextLoadThenCreditInspectorControllerNotNull() throws Exception {
        assertThat(creditInspectorControllerRE).isNotNull();
    }


    @Test
    public void whenContextLoadThenKafkaControllerNotNull() throws Exception {
        assertThat(kafkaControllerRE).isNotNull();
    }


    @Test
    public void whenContextLoadThenRegistrationControllerNotNull() throws Exception {
        assertThat(registrationControllerRE).isNotNull();
    }


    @Test
    public void whenContextLoadThenUnderWriterControllerNotNull() throws Exception {
        assertThat(underWriterControllerRE).isNotNull();
    }


    @Test
    public void whenContextLoadThenUserControllerNotNull() throws Exception {
        assertThat(userControllerRE).isNotNull();
    }
}
