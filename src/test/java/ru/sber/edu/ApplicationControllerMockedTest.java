package ru.sber.edu;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.sber.edu.v1.controllers.ApplicationControllerRE;
import ru.sber.edu.model.Application;
import ru.sber.edu.repo.ApplicationRepository;

import java.util.ArrayList;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@ContextConfiguration(classes = MockServletContext.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ApplicationControllerMockedTest {
    @InjectMocks
    ApplicationControllerRE applicationControllerRE;

    @Mock
    private ApplicationRepository applicationRepository;


    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(applicationControllerRE).build();
    }

    @Test
    public void testGetAllApplications() throws Exception {
        when(applicationRepository.findAll()).thenReturn(new ArrayList<Application>());
        mockMvc.perform(get("/v1/applications/all")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }






}
