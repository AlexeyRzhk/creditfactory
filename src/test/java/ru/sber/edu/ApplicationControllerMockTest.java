package ru.sber.edu;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import ru.sber.edu.v1.controllers.ApplicationControllerRE;
import ru.sber.edu.model.Application;
import ru.sber.edu.model.User;
import ru.sber.edu.repo.ApplicationRepository;
import ru.sber.edu.repo.ClientRepository;


import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class ApplicationControllerMockTest {

    @Mock
    private ApplicationRepository applicationRepository;

    @Mock
    private ClientRepository clientRepository;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void whenGetAllApplicationsThenGetOkStatusAndNotEmptyBody() {
        ApplicationControllerRE applicationControllerRE = new ApplicationControllerRE();
        ReflectionTestUtils.setField(applicationControllerRE, "applicationRepository", applicationRepository);
        when(applicationRepository.findAll()).thenReturn(new ArrayList<>());
        ResponseEntity<Iterable<Application>> allApplications = applicationControllerRE.all();
        verify(applicationRepository, times(1)).findAll();
        assertEquals(HttpStatus.OK, allApplications.getStatusCode());
        assertEquals(0, Lists.newArrayList(allApplications.getBody()).size());
    }

    @Test
    public void whenCreateApplicationThenGetCreatedStatus() {
        ApplicationControllerRE applicationControllerRE = new ApplicationControllerRE();
        ReflectionTestUtils.setField(applicationControllerRE, "applicationRepository", applicationRepository);
        ReflectionTestUtils.setField(applicationControllerRE, "clientRepository", clientRepository);
        when(applicationRepository.findAll()).thenReturn(new ArrayList<>());
        when(clientRepository.findByUsername("user")).thenReturn(new User(1L,null,null,null,null,null,null));
    }

}
