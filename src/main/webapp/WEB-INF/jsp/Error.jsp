<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>No money no honey</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<link rel="stylesheet" type="text/css" href="../../style_error.css">

</head>
<body>
<div class = "img">
	<img src="../../pig.jpg"
		 alt="свинка копилка">
</div>
<div class = "upside">
	<div class= "title">
		<h1><a href = "/">No money No honey</a></h1>
	</div>
	<div class= "subtitle">
		<h2>Выгодные кредиты за смешные проценты</h2>
	</div>
</div>
<div class = "listOfService">
	<ol>
		<li><h3>Кредиты</h3></li>
		<ol>
			<li><form action="/v2/applications" method="GET"><input  name="creditType" type="hidden" value="potreb"/>
				<input type="submit" value="потребительский"> </form></li>
			<li><form action="/v2/applications" method="GET"><input  name="creditType" type="hidden" value="ipoteka"/>
				<input type="submit" value="ипотека"> </form></li>
			<li><form action="/v2/applications" method="GET"><input  name="creditType" type="hidden" value="auto"/>
				<input type="submit" value="авто"> </form></li>
		</ol>
		<li><h3>Пользователи</h3></li>
		<ol>
			<li><form action="/v2/client" method="GET">
				<input type="submit" value="личный кабинет"> </form></li>
			<li><form action="/v2/creditInspector" method="GET">
				<input type="submit" value="кабинет инспектора"> </form></li>
			<li><form action="/v2/underwriter" method="GET">
				<input type="submit" value="кабинет страховщика"> </form></li>
			<li><form action="/v2/register" method="GET">
				<input type="submit" value="регистрация"> </form></li>
		</ol>

	</ol>
</div>
<div class = "error">
	<h3>Произошла ошибка</h3>
</div>


<footer class = "footer">
	Designed by webstudio (c) 2022
</footer>
</body>
</html>