package ru.sber.edu.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import ru.sber.edu.security.MyBasicAuthenticationEntryPoint;

import javax.inject.Inject;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Inject
    private MyBasicAuthenticationEntryPoint authenticationEntryPoint;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/v1/register").permitAll()
                .antMatchers("/v1/client").authenticated()
                .antMatchers("/v1/client/notifications").authenticated()
                .antMatchers("/v1/applications/create").authenticated()
                .antMatchers("/v1/client/create").authenticated()
                .antMatchers("/v1/admin").authenticated()
                .antMatchers("/v1/creditInspector").hasAuthority("CREDIT_INSPECTOR")
                .antMatchers("/v1/creditInspector/{id}/change").hasAuthority("CREDIT_INSPECTOR")
                .antMatchers("/v1/creditInspector/{id}").hasAuthority("CREDIT_INSPECTOR")
                .antMatchers("/v1/underwriter").hasAuthority("UNDERWRITER")
                .antMatchers("/v1/underwriter/{id}/change").hasAuthority("UNDERWRITER")
                .antMatchers("/v1/underwriter/{id}").hasAuthority("UNDERWRITER")
                .antMatchers("/v1/underwriter/notifications").hasAuthority("UNDERWRITER")
                .antMatchers("/v1/underwriter/checkTax").hasAuthority("UNDERWRITER")
                .antMatchers("/v1/client/all").hasAuthority("UNDERWRITER")
                .antMatchers("/v2/register").permitAll()
                .antMatchers("/v2/client").authenticated()
                .antMatchers("/v2/client/notifications").authenticated()
                .antMatchers("/v2/applications/create").authenticated()
                .antMatchers("/v2/client/create").authenticated()
                .antMatchers("/v2/admin").authenticated()
                .antMatchers("/v2/creditInspector").hasAuthority("CREDIT_INSPECTOR")
                .antMatchers("/v2/creditInspector/{id}/change").hasAuthority("CREDIT_INSPECTOR")
                .antMatchers("/v2/creditInspector/{id}").hasAuthority("CREDIT_INSPECTOR")
                .antMatchers("/v2/underwriter").hasAuthority("UNDERWRITER")
                .antMatchers("/v2/underwriter/{id}/change").hasAuthority("UNDERWRITER")
                .antMatchers("/v2/underwriter/{id}").hasAuthority("UNDERWRITER")
                .antMatchers("/v2/underwriter/notifications").hasAuthority("UNDERWRITER")
                .antMatchers("/v2/underwriter/checkTax").hasAuthority("UNDERWRITER")
                .antMatchers("/v2/client/all").hasAuthority("UNDERWRITER")
                .antMatchers("/resources/**").permitAll().anyRequest().permitAll()
                .and()
                .httpBasic().authenticationEntryPoint(authenticationEntryPoint).realmName("CreditFactory")
                .and()
                .csrf()
                .disable();


    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        return NoOpPasswordEncoder.getInstance();
    }

    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

}
