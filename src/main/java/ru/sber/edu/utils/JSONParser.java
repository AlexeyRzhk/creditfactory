package ru.sber.edu.utils;

import org.json.JSONObject;

public class JSONParser {
    public static String parse(String jsonString, String key){
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            return jsonObject.getString(key);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        throw new RuntimeException();
    }


}
