package ru.sber.edu.v2.controllers;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.sber.edu.ApplicationStatus;
import ru.sber.edu.model.Application;
import ru.sber.edu.model.Notification;
import ru.sber.edu.model.User;
import ru.sber.edu.repo.ApplicationRepository;
import ru.sber.edu.repo.ClientRepository;
import ru.sber.edu.repo.NotificationRepository;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping(value = "/v2/underwriter")
public class UnderWriterController {

    @Inject
    private ClientRepository clientRepository;

    @Inject
    private ApplicationRepository applicationRepository;

    @Inject
    private NotificationRepository notificationRepository;

    private static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");



    @GetMapping
    public ModelAndView getApprovedApplications(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("ApprovedApplications");
        ArrayList<Application> applications = new ArrayList<>();
        applicationRepository.findApplicationByStatus(ApplicationStatus.APPROVED.toString()).forEach(applications::add);

        StringBuilder builder = new StringBuilder();
        for(Application application: applications){
            User user = clientRepository.findById(application.getUser()).get();
            builder.append("id ").append(application.getId()).append(" ").append(" сумма ").append(application.getValue()).append(" процент ").append(application.getRate()).append(" срок ").append(application.getTerm()).append(" <br> id пользователя: ").append(application.getUser()).append(" пользователь ").append(user.getName()).append(" ").append(user.getSurname()).append(" статус ").append(application.getStatus()).append("<form action=\"/v2/underwriter/").append(application.getId()).append("\" method=\"GET\"><input type=\"submit\" value=\"инфо\"> </form>").append("<br>");
        }
        modelAndView.addObject("approvedApplications", builder.toString());
        return modelAndView;
    }

    @PostMapping(value = "/{id}/change")
    public ModelAndView changeApplicationStatus(HttpServletRequest request, @PathVariable Long id){
        ModelAndView modelAndView = new ModelAndView();
        try {
            modelAndView.setViewName("SuccessStatusChanged");
            Application application = applicationRepository.findById(id).get();
            ApplicationStatus applicationStatus = ApplicationStatus.valueOf(request.getParameter("status"));
            application.setStatus(applicationStatus.toString());
            applicationRepository.save(application);
            Notification notification = new Notification();
            notification.setUser(application.getUser());
            notification.setMessage("статус заявки № " + application.getId() + " изменен на " + application.getStatus());
            notification.setDate(dateFormat.format(new Date()));
            notificationRepository.save(notification);
            return modelAndView;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        modelAndView.setViewName("Error");
        return modelAndView;
    }


    @GetMapping(value = "/{id}")
    public ModelAndView getApplication(@PathVariable Long id){
        ModelAndView modelAndView = new ModelAndView();
        try {
            modelAndView.setViewName("ApprovedApplication");
            Application application = applicationRepository.findById(id).get();
            StringBuilder builder = new StringBuilder();
            builder.append("id ").append(application.getId()).append(" ").append(" сумма ").append(application.getValue()).append(" процент").append(application.getRate()).append(" срок ").append(application.getTerm()).append(" id пользователя: ").append(application.getUser()).append(" статус ").append(application.getStatus()).append("<form action=\"/v2/underwriter/").append(application.getId()).append("/change\" method=\"POST\"><input type=\"hidden\" name=\"status\" value=\"OK\"><input type=\"submit\" value=\"одобрить\"> </form>").append("<form action=\"/v2/underwriter/").append(application.getId()).append("/change\" method=\"POST\"><input type=\"hidden\" name=\"status\" value=\"DISAPPROVED\"><input type=\"submit\" value=\"отказать\"> </form>").append("<br>");
            modelAndView.addObject("approvedApplication", builder.toString());
            return modelAndView;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        modelAndView.setViewName("Error");
        return modelAndView;
    }

    @GetMapping(value = "notifications")
    public ModelAndView getNotifications(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("UnderwriterNotifications");
        ArrayList<Notification> notifications = new ArrayList<>();
        notificationRepository.findByUser(KafkaController.UNDERWRITER_NOTIFICATIONS_ID).forEach(notifications::add);
        StringBuilder builder = new StringBuilder();
        for(Notification notification: notifications){
            builder.append(notification.getDate()).append(" ").append(notification.getMessage()).append("<br>");
        }
        modelAndView.addObject("notifications", builder.toString());
        return modelAndView;
    }

}
