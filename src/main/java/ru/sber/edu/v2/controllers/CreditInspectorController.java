package ru.sber.edu.v2.controllers;


import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.sber.edu.ApplicationStatus;
import ru.sber.edu.model.Application;
import ru.sber.edu.model.Notification;
import ru.sber.edu.repo.ApplicationRepository;
import ru.sber.edu.repo.ClientRepository;
import ru.sber.edu.repo.NotificationRepository;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping(value = "/v2/creditInspector")
public class CreditInspectorController {

    @Inject
    private ClientRepository clientRepository;

    @Inject
    private ApplicationRepository applicationRepository;

    @Inject
    private NotificationRepository notificationRepository;

    private static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");


    @GetMapping
    public ModelAndView getCreatedApplications(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("CreatedApplications");
        ArrayList<Application> applications = new ArrayList<>();
        applicationRepository.findApplicationByStatus(ApplicationStatus.CREATED.toString()).forEach(applications::add);
        StringBuilder builder =new StringBuilder();
        for(Application application: applications){
            builder.append("id ").append(application.getId()).append(" ").append(" сумма ").append(application.getValue()).append(" процент ").append(application.getRate()).append(" срок ").append(application.getTerm()).append(" id пользователя: ").append(application.getUser()).append(" статус ").append(application.getStatus()).append(" <form action=\"/v2/creditInspector/").append(application.getId()).append("\" method=\"GET\"><input type=\"submit\" value=\"инфо\"> </form>").append("<br>");
        }
        modelAndView.addObject("createdApplications", builder.toString());
        return modelAndView;
    }

    @PostMapping(value = "/{id}/change")
    public ModelAndView changeApplicationStatus(HttpServletRequest request, @PathVariable Long id){
        ModelAndView modelAndView = new ModelAndView();
        try {
            Application application = applicationRepository.findById(id).get();
            ApplicationStatus applicationStatus = ApplicationStatus.valueOf(request.getParameter("status"));
            application.setStatus(applicationStatus.toString());
            applicationRepository.save(application);
            Notification notification = new Notification();
            notification.setUser(application.getUser());
            notification.setMessage("статус заявки № " + application.getId() + " изменен на " + application.getStatus());
            notification.setDate(dateFormat.format(new Date()));
            notificationRepository.save(notification);
            modelAndView.setViewName("SuccessStatusChanged");
            return modelAndView;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        modelAndView.setViewName("Error");
        return modelAndView;
    }


    @GetMapping(value = "/{id}")
    public ModelAndView getApplication(@PathVariable Long id){
        ModelAndView modelAndView = new ModelAndView();
        try {
            modelAndView.setViewName("CreatedApplication");
            Application application = applicationRepository.findById(id).get();
            StringBuilder builder = new StringBuilder();
            builder.append("id ").append(application.getId()).append(" ").append(" сумма ").append(application.getValue()).append(" процент ").append(application.getRate()).append(" срок ").append(application.getTerm()).append(" id пользователя: ").append(application.getUser()).append(" статус ").append(application.getStatus()).append("<form action=\"/v2/creditInspector/").append(application.getId()).append("/change\" method=\"POST\"><input type=\"hidden\" name=\"status\" value=\"APPROVED\"><input type=\"submit\" value=\"одобрить\"> </form>").append("<form action=\"/v2/creditInspector/").append(application.getId()).append("/change\" method=\"POST\"><input type=\"hidden\" name=\"status\" value=\"DISAPPROVED\"><input type=\"submit\" value=\"отказать\"> </form>").append("<br>");
            modelAndView.addObject("createdApplication", builder.toString());
            return modelAndView;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        modelAndView.setViewName("Error");
        return modelAndView;
    }











}
