package ru.sber.edu.v2.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ru.sber.edu.model.Application;
import ru.sber.edu.model.Notification;
import ru.sber.edu.model.User;
import ru.sber.edu.repo.ApplicationRepository;
import ru.sber.edu.repo.ClientRepository;
import ru.sber.edu.repo.NotificationRepository;

import javax.inject.Inject;
import java.security.Principal;
import java.util.ArrayList;


@RestController
@RequestMapping(value = "/v2/client")
public class UserController {

    @Inject
    ClientRepository userRepository;

    @Inject
    ApplicationRepository applicationRepository;

    @Inject
    NotificationRepository notificationRepository;



    @GetMapping
    public ModelAndView getUserInfo(Principal principal){
        ModelAndView modelAndView = new ModelAndView();
        try{
        User user = userRepository.findByUsername(principal.getName());
        modelAndView.setViewName("/Client");
        modelAndView.addObject("name", user.getName());
        modelAndView.addObject("surname", user.getSurname());
        modelAndView.addObject("income", user.getIncome());
        return modelAndView;}
        catch (Exception e){
            e.printStackTrace();
        }
        modelAndView.setViewName("Main");
        modelAndView.setStatus(HttpStatus.UNAUTHORIZED);
        return modelAndView;
    }


    @GetMapping(value = "applications")
    public ModelAndView getUserApplications(Principal principal){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/ClientApplications");
        User user = userRepository.findByUsername(principal.getName());
        ArrayList<Application> applications = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        applicationRepository.findByUser(user.getId()).forEach(applications::add);
        for(Application application: applications){
            builder.append(applicationToString(application));
        }
        modelAndView.addObject("applications", builder.toString());
        return modelAndView;
    }

    @GetMapping(value = "/applications/{id}")
    public ModelAndView getUserApplication(@PathVariable Long id, Principal principal){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/ClientApplication");
            Application application = applicationRepository.findById(id).get();
            User user = userRepository.findByUsername(principal.getName());
            modelAndView.addObject("application", applicationToString(application));
            if(user.getId().equals(application.getUser())) {
                return modelAndView;
            }
            throw new RuntimeException("неправильный номер заявки");

    }

    @GetMapping(value = "/notifications")
    public ModelAndView getUserNotifications(Principal principal) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/ClientNotifications");
        User user = userRepository.findByUsername(principal.getName());
        ArrayList<Notification> notifications = new ArrayList<>();
        StringBuilder builder = new StringBuilder();
        notificationRepository.findByUser(user.getId()).forEach(notifications::add);
        for(Notification notification: notifications){
            builder.append(notificationToString(notification));
        }
        modelAndView.addObject("notifications", builder.toString());
        return modelAndView;
    }

    @GetMapping(value = "/notifications/{id}")
    public ModelAndView getUserNotification(@PathVariable Long id, Principal principal){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/ClientNotification");
        Notification notification = notificationRepository.findById(id).get();
        User user = userRepository.findByUsername(principal.getName());
        modelAndView.addObject("notification", notificationToString(notification));
        if(user.getId().equals(notification.getUser())) {
            return modelAndView;
        }

        throw new RuntimeException("неправильный номер оповещения");
    }



    @GetMapping("/all")
    public ResponseEntity<Iterable<User>> all(){
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }

    private String applicationToString(Application application){
        return "id: " +
                application.getId() +
                " статус: " +
                application.getStatus() +
                "<br>" + "сумма: " + application.getValue() + " процент: " + application.getRate() + " срок: " + application.getTerm() + " тип кредита " + application.getCreditType() +  " <br>";
    }

    private String notificationToString(Notification notification){
        return "дата: " +
                notification.getDate() +
                " сообщение: " +
                notification.getMessage() +
                " id оповещения " +
                notification.getNotificationId() +
                "<br>";
    }





}
