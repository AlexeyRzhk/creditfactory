package ru.sber.edu.v2.controllers;


import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import ru.sber.edu.model.Notification;
import ru.sber.edu.model.User;
import ru.sber.edu.repo.ClientRepository;
import ru.sber.edu.repo.NotificationRepository;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;


@RestController()
@RequestMapping(value = "/v2")
@EnableKafka
public class KafkaController {

    public static final Long UNDERWRITER_NOTIFICATIONS_ID = -3L;

    @Inject
    KafkaTemplate<Integer, String> template;

    @Inject
    ClientRepository userRepository;

    @Inject
    NotificationRepository notificationRepository;

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");


    @GetMapping(value = "/underwriter/checkTaxForm")
    public ModelAndView formToSendMessage(){
        ModelAndView modelAndView = new ModelAndView();
        try {
            modelAndView.setViewName("TaxCheck");
            return modelAndView;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        modelAndView.setViewName("Error");
        return modelAndView;
    }

    @GetMapping(value = "/underwriter/checkTax")
    public ModelAndView sendMessage(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        try {
            modelAndView.setViewName("Success");
            Optional<User> oUser = userRepository.findById(Long.parseLong(request.getParameter("userId")));
            User user = oUser.get();
            template.send("topic1", user.getName() + " " + user.getSurname());
            return modelAndView;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        modelAndView.setViewName("Error");
        return modelAndView;
    }



}
