package ru.sber.edu.v2.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;


@RestController
@RequestMapping(value = "/")
public class AdminController {



    @GetMapping
    public ModelAndView MainPage(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/Main");
        modelAndView.addObject("message", "yeeeeeea!");
        return modelAndView;
    }

}
