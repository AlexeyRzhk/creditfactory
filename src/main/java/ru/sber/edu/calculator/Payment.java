package ru.sber.edu.calculator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Payment {
    private double value;
    private Date date;
    private DateFormat format = new SimpleDateFormat("dd-MM-yyyy");

    public double getValue() {
        return value;
    }

    public Date getDate() {
        return date;
    }

    public Payment(Date date, double value) {
        this.value = value;
        this.date = date;
    }

    @Override
    public String toString() {
        return "value=" + value +
                " date=" + format.format(date);
    }
}
