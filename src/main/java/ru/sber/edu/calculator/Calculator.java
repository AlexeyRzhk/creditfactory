package ru.sber.edu.calculator;

public class Calculator {

    public static double annuiteMonthlyPayment(double S, double P, int m){
        double p = (P / 12) / 100;
        double ka = (p * Math.pow(1 + p, m)) / (Math.pow(1 + p, m) - 1);
        double X = S * ka;
        return round(X);
    }

    private static double round(double roundNumber){
        String roundedString = String.format("%.2f", roundNumber);
        roundedString = roundedString.replace(',','.');
        return Double.parseDouble(roundedString);
    }
}
