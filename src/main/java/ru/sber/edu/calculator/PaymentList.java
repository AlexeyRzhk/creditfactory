package ru.sber.edu.calculator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class PaymentList {
    List<Payment> payments;

    public PaymentList(int term, double value, double rate) {
        payments = new ArrayList<>();
        fillPayments(term, value, rate);
    }

    private void fillPayments(int term, double value, double rate){
        Calendar calendar = GregorianCalendar.getInstance();
        for (int i = 1;i <= term;i++){
            calendar.add(Calendar.MONTH, 1);
            Payment payment = new Payment(calendar.getTime(), Calculator.annuiteMonthlyPayment(value, rate, term));
            payments.add(payment);
        }
    }


    public List<Payment> getPayments() {
        return payments;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("payments\n");
        for(Payment payment: payments){
            result.append(payment.toString()).append("\n");
        }
        return result.toString();
    }
}
