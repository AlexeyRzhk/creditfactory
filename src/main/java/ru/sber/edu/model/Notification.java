package ru.sber.edu.model;


import javax.persistence.*;

@Entity
public class Notification {

    @Id
    @GeneratedValue
    @Column(name = "notification_id")
    Long notificationId;

    @Column(name = "message")
    private String message;

    @Column
    private Long user;

    @Column(name = "date")
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(Long notificationId) {
        this.notificationId = notificationId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long user) {
        this.user = user;
    }
}
