package ru.sber.edu.model;

import javax.persistence.*;
@Entity
public class Application {
    @Id
    @GeneratedValue
    @Column(name = "application_id")
    private Long id;

    @Column
    private Long user;

    @Column(name = "credit_type")
    private String creditType;

    @Column(name = "value")
    private String value;

    @Column(name = "rate")
    private String rate;

    @Column(name = "term")
    private String term;

    @Column(name = "status")
    private String status;

    @Override
    public String toString() {
        return "Application{" +
                "id=" + id +
                ", user=" + user +
                ", creditType='" + creditType + '\'' +
                ", value='" + value + '\'' +
                ", rate='" + rate + '\'' +
                ", term='" + term + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getUser() {
        return user;
    }

    public void setUser(Long userId) {
        this.user = userId;
    }
}
