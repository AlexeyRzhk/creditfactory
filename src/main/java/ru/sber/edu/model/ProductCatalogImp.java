package ru.sber.edu.model;

import java.util.List;

public class ProductCatalogImp implements ProductCatalog{

    private List<Product> products;

    @Override
    public boolean addProduct(Product product){
        return products.add(product);
    }

    @Override
    public List<Product> getProducts(){
        return products;
    }


}
