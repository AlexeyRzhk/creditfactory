package ru.sber.edu.model;

import java.util.List;

public interface ProductCatalog {

    public boolean addProduct(Product product);

    public List<Product> getProducts();
}
