package ru.sber.edu.model;


import org.springframework.lang.Nullable;

import javax.persistence.Id;

import javax.persistence.*;
import java.util.Set;

@Entity
public class User {
    @Id
    @GeneratedValue
    @Column(name = "user_id")
    protected Long id;
    @Column(name = "name")
    protected String name;
    @Column(name = "surname")
    protected String surname;
    @Column(name = "income")
    protected String income;
    @Column(nullable = false, unique = true)
    protected String username;
    @Column
    protected String password;
    @Column
    protected String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {

        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", income='" + income + '\'' +
                '}';
    }

    public User() {
    }

    public User(@Nullable Long id, @Nullable String name, @Nullable String surname, @Nullable String income, @Nullable String username, @Nullable String password, @Nullable String role) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.income = income;
        this.username = username;
        this.password = password;
        this.role = role;
    }
}
