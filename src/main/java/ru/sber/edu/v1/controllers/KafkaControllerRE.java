package ru.sber.edu.v1.controllers;


import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.sber.edu.model.Notification;
import ru.sber.edu.model.User;
import ru.sber.edu.repo.ClientRepository;
import ru.sber.edu.repo.NotificationRepository;
import ru.sber.edu.utils.JSONParser;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;


@RestController()
@RequestMapping(value = "/v1")
@EnableKafka
public class KafkaControllerRE {

    public static final Long UNDERWRITER_NOTIFICATIONS_ID = -3L;

    @Inject
    KafkaTemplate<Integer, String> template;

    @Inject
    ClientRepository userRepository;

    @Inject
    NotificationRepository notificationRepository;

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    @Bean
    public NewTopic topic() {
        return TopicBuilder.name("topic1")
                .partitions(10)
                .replicas(1)
                .build();
    }

    @KafkaListener(id = "my2Id", topics = "topic2")
    public void listen(String in) {
        System.out.println(in);
        Notification notification = new Notification();
        notification.setDate(DATE_FORMAT.format(new Date()));
        notification.setUser(UNDERWRITER_NOTIFICATIONS_ID);
        notification.setMessage(in);
        notificationRepository.save(notification);
    }

    @GetMapping(value = "/underwriter/checkTax")
    public ResponseEntity<?> sendMessage(@RequestBody String userId){
        try {
            Optional<User> oUser = userRepository.findById(Long.parseLong(JSONParser.parse(userId, "userId")));
            User user = oUser.get();
            template.send("topic1", user.getName() + " " + user.getSurname());
            return new ResponseEntity<>("ok", null, HttpStatus.OK);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(null, null, HttpStatus.BAD_REQUEST);
    }
}
