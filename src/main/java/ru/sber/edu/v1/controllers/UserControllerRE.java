package ru.sber.edu.v1.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.sber.edu.model.Application;
import ru.sber.edu.model.Notification;
import ru.sber.edu.model.User;
import ru.sber.edu.repo.ApplicationRepository;
import ru.sber.edu.repo.ClientRepository;
import ru.sber.edu.repo.NotificationRepository;

import javax.inject.Inject;
import java.security.Principal;
import java.util.ArrayList;




@RestController
@RequestMapping(value = "/v1/client")
public class UserControllerRE {

    @Inject
    ClientRepository userRepository;

    @Inject
    ApplicationRepository applicationRepository;

    @Inject
    NotificationRepository notificationRepository;



    @GetMapping
    public ResponseEntity<?> getUserInfo(Principal principal){
        try {
            User user = userRepository.findByUsername(principal.getName());
            return new ResponseEntity<>(user, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
    }


    @GetMapping(value = "applications")
    public ResponseEntity<?> getUserApplications(Principal principal){

        User user = userRepository.findByUsername(principal.getName());
        ArrayList<Application> applications = new ArrayList<>();

        applicationRepository.findByUser(user.getId()).forEach(applications::add);
        return new ResponseEntity<>(applications, HttpStatus.OK);
    }

    @GetMapping(value = "/applications/{id}")
    public ResponseEntity<?> getUserApplication(@PathVariable Long id, Principal principal){

            Application application = applicationRepository.findById(id).get();
            User user = userRepository.findByUsername(principal.getName());

            if(user.getId().equals(application.getUser())) {
                return new ResponseEntity<>(application, HttpStatus.OK);
            }
            throw new RuntimeException("неправильный номер заявки");

    }

    @GetMapping(value = "/notifications")
    public ResponseEntity<?> getUserNotifications(Principal principal) {
        User user = userRepository.findByUsername(principal.getName());
        ArrayList<Notification> notifications = new ArrayList<>();
        notificationRepository.findByUser(user.getId()).forEach(notifications::add);
        return new ResponseEntity<>(notifications, HttpStatus.OK);
    }

    @GetMapping(value = "/notifications/{id}")
    public ResponseEntity<?> getUserNotification(@PathVariable Long id, Principal principal){

        Notification notification = notificationRepository.findById(id).get();
        User user = userRepository.findByUsername(principal.getName());

        if(user.getId().equals(notification.getUser())) {
            return new ResponseEntity<>(notification, HttpStatus.OK);
        }

        throw new RuntimeException("неправильный номер оповещения");
    }



    @GetMapping("/all")
    public ResponseEntity<Iterable<User>> all(){
        return new ResponseEntity<>(userRepository.findAll(), HttpStatus.OK);
    }





}
