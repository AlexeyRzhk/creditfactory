package ru.sber.edu.v1.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.sber.edu.ApplicationStatus;
import ru.sber.edu.model.Application;
import ru.sber.edu.model.Notification;
import ru.sber.edu.repo.ApplicationRepository;
import ru.sber.edu.repo.NotificationRepository;
import ru.sber.edu.utils.JSONParser;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@RestController
@RequestMapping(value = "/v1/underwriter")
public class UnderWriterControllerRE {

    @Inject
    private ApplicationRepository applicationRepository;

    @Inject
    private NotificationRepository notificationRepository;

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    @GetMapping
    public ResponseEntity<?> getCreatedApplications() {
        ArrayList<Application> applications = new ArrayList<>();
        applicationRepository.findApplicationByStatus(ApplicationStatus.APPROVED.toString()).forEach(applications::add);
        return new ResponseEntity<>(applications, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}/change")
    public ResponseEntity<?> changeApplicationStatus(@RequestBody String status, @PathVariable Long id){
        try {
            Application application = applicationRepository.findById(id).get();
            ApplicationStatus applicationStatus = ApplicationStatus.valueOf(JSONParser.parse(status, "status"));
            application.setStatus(applicationStatus.toString());
            applicationRepository.save(application);
            Notification notification = new Notification();
            notification.setUser(application.getUser());
            notification.setMessage("статус заявки № " + application.getId() + " изменен на " + application.getStatus());
            notification.setDate(DATE_FORMAT.format(new Date()));
            notificationRepository.save(notification);
            return new ResponseEntity<>(null, HttpStatus.CREATED);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }


    @GetMapping(value = "/{id}")
    public ResponseEntity<?> getApplication(@PathVariable Long id){
        try {
            Application application = applicationRepository.findById(id).get();
            return new ResponseEntity<>(application, HttpStatus.OK);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = "notifications")
    public ResponseEntity<?> getNotifications(){
        ArrayList<Notification> notifications = new ArrayList<>();
        notificationRepository.findByUser(KafkaControllerRE.UNDERWRITER_NOTIFICATIONS_ID).forEach(notifications::add);
        return new ResponseEntity<>(notifications, HttpStatus.OK);
    }

}
