package ru.sber.edu.v1.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.sber.edu.ApplicationStatus;
import ru.sber.edu.calculator.Payment;
import ru.sber.edu.calculator.PaymentList;
import ru.sber.edu.model.Application;
import ru.sber.edu.repo.ApplicationRepository;
import ru.sber.edu.repo.ClientRepository;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.security.Principal;


@RestController
@RequestMapping(value = "/v1/applications")
public class ApplicationControllerRE {

    @Inject
    ClientRepository clientRepository;

    @Inject
    ApplicationRepository applicationRepository;

    @GetMapping
    public ModelAndView creditCalculation(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("creditType", request.getParameter("creditType"));
        modelAndView.setViewName("CreateApplicationForm");
        return modelAndView;
    }

    @GetMapping(value = "/calc")
    public ModelAndView creditCalcResult(HttpServletRequest request){
        try {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.setViewName("CreditCalculation");
            PaymentList paymentList = new PaymentList(
                    Integer.parseInt(request.getParameter("term")),
                    Double.parseDouble(request.getParameter("value")),
                    Double.parseDouble(request.getParameter("rate")));
            StringBuilder builder = new StringBuilder();
            builder.append("<table>");
            for(Payment payment: paymentList.getPayments()){
                builder.append("<tr>")
                        .append("<td>").append(payment.getDate()).append("</td>")
                        .append("<td>").append(payment.getValue()).append("</td>")
                        .append("</tr>");
            }
            builder.append("</table>");
            modelAndView.addObject("creditType", request.getParameter("creditType"));
            modelAndView.addObject("value", request.getParameter("value"));
            modelAndView.addObject("rate", request.getParameter("rate"));
            modelAndView.addObject("term", request.getParameter("term"));
            modelAndView.addObject("tableOfPayments", builder.toString());
            return modelAndView;
        }catch (Exception e){
            e.printStackTrace();
        }
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Error");
        return modelAndView;
    }

    @PostMapping(value = "/create")
    public ResponseEntity<?> create(@RequestBody Application application, Principal principal){

        application.setStatus(ApplicationStatus.CREATED.toString());
        application.setUser(clientRepository.findByUsername(principal.getName()).getId());
        applicationRepository.save(application);
        System.out.println(application);

        return new ResponseEntity<>(null, HttpStatus.CREATED);
    }





    @GetMapping("/all")
    public ResponseEntity<Iterable<Application>> all(){
        Iterable<Application> entity = applicationRepository.findAll();

        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    /*@GetMapping("/applications/{userId}")
    public ResponseEntity<Iterable<Application>> findById(@PathVariable Long userId){
        Iterable<Application> entity = applicationRepository.findAll();
        List<Application> applications = new LinkedList<>();
        for (Application application :entity) {
            if(application.getUser().equals(userId)){
                applications.add(application);
            }
        }

        return new ResponseEntity<>(applications, HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/applications/create")
    public ResponseEntity<?> create(@RequestBody Application application){
        application.setStatus(ApplicationStatus.CREATED.toString());
        applicationRepository.save(application);
        System.out.println(application);

        HttpHeaders responseHeaders = new HttpHeaders();
        URI newVoteUri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(application.getId())
                .toUri();
        responseHeaders.setLocation(newVoteUri);


        return new ResponseEntity<>(null,responseHeaders, HttpStatus.CREATED);
    }*/


}
