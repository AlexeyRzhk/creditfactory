package ru.sber.edu.v1.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.sber.edu.model.User;
import ru.sber.edu.repo.ClientRepository;

import javax.inject.Inject;

@RestController
@RequestMapping(value = "/v1/register")
public class RegistrationControllerRE {

    @Inject
    ClientRepository clientRepository;

    @GetMapping
    public ModelAndView registerView(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("Register");
        return modelAndView;
    }


    @PostMapping
    public ResponseEntity<?> register(@RequestBody User user){
        if(isUserExists(user)){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
        user.setRole("USER");
        clientRepository.save(user);
        return new ResponseEntity<>(null, HttpStatus.OK);
    }

    public boolean isUserExists(User user){
        return clientRepository.findByUsername(user.getUsername()) != null;
    }

}
