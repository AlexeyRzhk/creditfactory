package ru.sber.edu.repo;

import org.springframework.data.repository.CrudRepository;

import ru.sber.edu.model.User;

public interface ClientRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);




}
