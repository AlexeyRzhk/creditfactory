package ru.sber.edu.repo;

import org.springframework.data.repository.CrudRepository;
import ru.sber.edu.model.Notification;


public interface NotificationRepository extends CrudRepository<Notification, Long> {

    Iterable<Notification> findByUser(Long id);

}
