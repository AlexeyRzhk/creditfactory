package ru.sber.edu.repo;

import org.springframework.data.repository.CrudRepository;

import ru.sber.edu.model.Application;
import ru.sber.edu.model.Notification;

public interface ApplicationRepository extends CrudRepository<Application, Long> {

    Iterable<Application> findApplicationByStatus(String status);

    Iterable<Application> findByUser(Long id);

}
