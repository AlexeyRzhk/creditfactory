package ru.sber.edu.logger;


import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
public class LoggerAspect {

    @Pointcut("execution(* ru.sber.edu.v2.controllers.RegistrationController.*(..))")
    public void registerRestControllerPointCut(){

    }

    @Before("registerRestControllerPointCut()")
    public void beforeMethod(JoinPoint joinPoint){
        System.out.println("метод " + joinPoint.getSignature().getName() + " запрошен");
        System.out.println("со следующими аргументами: " + Arrays.toString(joinPoint.getArgs()));
    }

    @After("registerRestControllerPointCut()")
    public void afterMethod(JoinPoint joinPoint){
        System.out.println("исполнение метода " + joinPoint.getSignature().getName() + " завершено");
    }


}
